\subsection{Data structures}
\hl{data structures (buffers) within the control subsystem of the agent}

As one can see in \autoref{fig:agents}, the control subsystem $c_1$ has seven
different buffers. Four of them are related to the effector, one to the
receptor, one is the internal memory and another is related to communication
with an external agent.

The internal memory buffer $\leftidx{^c}{c}{_1}$ is defined in
\autoref{table:memory}. The buffers related to the 6dof manipulator $e_{1,m}$
($\leftidx{^e_y}{c}{_{1,m}}$ and $\leftidx{^e_x}{c}{_{1,m}}$) are defined in
\autoref{table:manipulator-buffers} and the ones related to the two-state
gripper $e_{1,g}$ ($\leftidx{^e_y}{c}{_{1,g}}$ and $\leftidx{^e_x}{c}{_{1,g}}$)
in \autoref{table:gripper-buffers}. The buffer related to the Kinect sensor
$r_{1,k}$ ($\leftidx{^r_x}{c}{_{1,k}}$) is defined in
\autoref{table:kinect-buffers} and the buffer related to the communication with
the external agent that informs about the palette state
($\leftidx{^T_x}{c}{_{1,p}}$) is defined in
\autoref{table:communication-buffers}.

\begin{table}
\centering
\begin{tabular}{rcp{0.6\linewidth}}
 & & \textbf{System configuration} \\
\hline
$\leftidx{^c}{c}{_1}[\leftidx{^B_K}{T}]$ & $-$ & Pose of the Kinect sensor (K)
    with respect to the robot base (B) reference frame\\
$\leftidx{^c}{c}{_1}[\leftidx{^B_P}{T}]$ & $-$ & Pose of the palette base (P)
    with respect to the robot base (B) reference frame. The palette base is
    defined as the \hl{WIP}\\
$\leftidx{^c}{c}{_1}[\leftidx{^B_{E_s}}{T}]$ & $-$ & Initial (start) pose of 
    the manipulator w.r.t. the robot base\\
$\leftidx{^c}{c}{_1}[\leftidx{^T_A}{T}]$ & $-$ & Constant transformation
    between a target pose (the pick or place for example) and its approach pose
    (the previous position where a force-controlled approach will start) \\
$\leftidx{^c}{c}{_1}[\dot{q}_m]$ & $-$ & Maximum joint rotation speed array.
    Contains the maximum allowed angular velocities for each of the joints of
    the manipulator \hl{WIP}\\
$\leftidx{^c}{c}{_1}[\leftidx{_z}{V}{_d}, \leftidx{_z}{D}{_d}, 
    \leftidx{_z}{I}{_d}]$ & $-$ & Values of the velocity, damping and inertia
    (along the Z axis of the end-effector reference frame), used during the
    force-monitored approach to the object \\
\\ & & \textbf{Effectors constant parameters} \\
\hline
$\leftidx{^c}{c}{_1}[d_m]$ & $-$ & Maximum distance between the two fingers of
    the gripper (when the gripper is fully open)\\
$\leftidx{^c}{c}{_1}[c_l]$ & $-$ & Limit of the measured gripper motor current,
    indicating that the fingertips have tightened on the object \\
$\leftidx{^c}{c}{_1}[\leftidx{^E_G}{T}]$ & $-$ & Constant transformation
    representing the pose of the center of the gripper (G) w.r.t. end-effector
    (E) \\
\\ & & \textbf{Task configuration parameters} \\
\hline
$\leftidx{^c}{c}{_1}[p_x]$ & $-$ & Palette width in meters\\
$\leftidx{^c}{c}{_1}[p_y]$ & $-$ & Palette length in meters\\
$\leftidx{^c}{c}{_1}[n_x]$ & $-$ & Number of objects to be distributed in a
single row in the palette\\
$\leftidx{^c}{c}{_1}[n_y]$ & $-$ & Number of objects to be distributed in a
single column in the palette\\
\\ & & \textbf{Task variables} \\
\hline
$\leftidx{^c}{c}{_1}[n_o]$ & $-$ & Number of objects in the palette\\
$\leftidx{^c}{c}{_1}[\leftidx{^B_{1..V}}{O}{_c}]$ & $-$ & A list of verified
    object hypotheses w.r.t. the robot base\\
$\leftidx{^c}{c}{_1}[d_o]$ & $-$ & Desired distance between the two fingers of
    the gripper, calculated on the basis of object dimensions\\
$\leftidx{^c}{c}{_1}[\leftidx{^B_{E_a}}{T}]$ & $-$ & Target approach pose.
    Desired pose of the end-effector (E) w.r.t. robot base reference frame (B)
    indicating where the force-controlled target approach phase will begin\\
$\leftidx{^c}{c}{_1}[\leftidx{^B_{E_t}}{T}]$ & $-$ & Target pose. Desired pose
    of the end-effector (E) w.r.t. robot base reference frame (B) in which the
    target position has been reached and a contact with an object might occur\\
\end{tabular}
\caption{Memory of the control subsystem}
\label{table:memory}
\end{table}

\begin{table}
\centering
\begin{tabular}{rcp{0.6\linewidth}}
 & & \textbf{Output to the manipulator $e_{1,m}$ for Point To Point motion} \\
\hline
$\leftidx{^e_y}{c}{_{1,m}}[\leftidx{^B_E}{T}{_d}]$ & $-$ & Desired Cartesian
    pose of the end-effector (E) w.r.t. the robot base (B) (homogeneous
    matrix)\\
\\ & & \textbf{Input from the manipulator $e_{1,m}$} \\
$\leftidx{^e_x}{c}{_{1,m}}[\leftidx{^B_E}{T}{_c}]$ & $-$ & Current Cartesian
    pose of the end-effector E w.r.t. the robot base (B) (homogeneous matrix)\\
$\leftidx{^e_x}{c}{_{1,m}}[\leftidx{^E}{F}{_c}]$ & $-$ & Current force/torque
    exerted by the end-effector w.r.t. the end-effector reference frame E\\
\\
\end{tabular}
\caption{Control subsystem's buffers related to the 6dof manipulator}
\label{table:manipulator-buffers}
\end{table}

\begin{table}
\centering
\begin{tabular}{rcp{0.6\linewidth}}
 & & \textbf{Output to gripper $e_{1,g}$} \\
\hline
$\leftidx{^e_y}{c}{_{1,g}}[d_d]$ & $-$ & 
    Desired gripper pose (distance between the jaws)\\
\\ & & \textbf{Input from gripper $e_{1,g}$} \\
\hline
$\leftidx{^e_x}{c}{_{1,g}}[d_c]$ & $-$ & 
    Current distance between the gripper jaws\\
$\leftidx{^e_x}{c}{_{1,g}}[c_c]$ & $-$ & 
    Measured motor current, proportional to the force exerted by the jaws on
    the grasped object\\
\\
\end{tabular}
\caption{Control subsystem's buffers related to the two-state gripper}
\label{table:gripper-buffers}
\end{table}

\begin{table}
\centering
\begin{tabular}{rcp{0.6\linewidth}}
 & & \textbf{Input from the Kinect sensor $r_{1,k}$} \\
\hline
$\leftidx{^r_x}{c}{_{1,k}}[WIP]$ & $-$ & 
    WIP\\
\\
\end{tabular}
\caption{Control subsystem's buffers related to the Kinect sensor}
\label{table:kinect-buffers}
\end{table}

\begin{table}
\centering
\begin{tabular}{rcp{0.6\linewidth}}
 & & \textbf{Input from the external agent that informs about the palette state} \\
\hline
$\leftidx{^T_x}{c}{_{1,p}}[START]$ & $-$ & 
    WIP\\
\\
\end{tabular}
\caption{Control subsystem's buffers related to communication with external agents}
\label{table:communication-buffers}
\end{table}