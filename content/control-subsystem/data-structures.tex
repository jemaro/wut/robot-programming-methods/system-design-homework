\subsection{Data structures}
\label{subsec:data-structures}

One can find in this section the data structures (buffers) within the control
subsystem of the agent defined in \autoref{sec:agent}.

As one can see in \autoref{fig:agents}, the control subsystem $c_1$ has seven
different buffers. Four of them are related to the effector, one to the
receptor, one is the internal memory and another is related to communication
with an external agent.

The internal memory buffer $\leftidx{^c}{c}{_1}$ is defined in
\autoref{table:memory}. The buffers related to the 6dof manipulator $e_{1,m}$
($\leftidx{^e_y}{c}{_{1,m}}$ and $\leftidx{^e_x}{c}{_{1,m}}$) are defined in
\autoref{table:manipulator-buffers} and the ones related to the two-state
gripper $e_{1,g}$ ($\leftidx{^e_y}{c}{_{1,g}}$ and $\leftidx{^e_x}{c}{_{1,g}}$)
in \autoref{table:gripper-buffers}. The buffer related to the Kinect sensor
$r_{1,k}$ ($\leftidx{^r_x}{c}{_{1,k}}$) is defined in
\autoref{table:kinect-buffers} and the buffer related to the communication with
the external agent that informs about the palette state
($\leftidx{^T_x}{c}{_{1,p}}$) is defined in
\autoref{table:communication-buffers}.


\begin{table}
\centering
\begin{tabular}{rcp{0.6\linewidth}}
 & & \textbf{System configuration} \\
\hline
$\leftidx{^c}{c}{_1}[\leftidx{^B_K}{T}]$ & $-$ & Pose of the Kinect sensor (K)
    with respect to the robot base (B) reference frame\\
$\leftidx{^c}{c}{_1}[\leftidx{^B_P}{T}]$ & $-$ & Pose of the palette base (P)
    with respect to the robot base (B) reference frame. The palette base is
    defined as the bottom left corner of the palette when seen from above. The
    $x$ and $y$ axis in its positive direction must match the sides of the
    palette while the positive $z$ is the direction towards which the objects
    will be pilled up (typically opposite to the ground direction). One can
    look at \autoref{fig:sketch} to have a better understanding of this
    definition\\
$\leftidx{^c}{c}{_1}[\leftidx{^B_{E_s}}{T}]$ & $-$ & Initial (start) pose of 
    the manipulator w.r.t. the robot base\\
$\leftidx{^c}{c}{_1}[\leftidx{^A_T}{T}]$ & $-$ & Constant transformation
    between a target pose (the pick or place grasp pose for example) and its
    approach pose (the previous position where a force-controlled approach will
    start) \\
$\leftidx{^c}{c}{_1}[\leftidx{_z}{V}{_d}, \leftidx{_z}{D}{_d}, 
    \leftidx{_z}{I}{_d}]$ & $-$ & Values of the velocity, damping and inertia
    (along the Z axis of the end-effector reference frame), used during the
    force-monitored approach to the object \\
$\leftidx{^c}{c}{_1}[\leftidx{_x}{D}{_d}, \leftidx{_y}{D}{_d},
    \leftidx{_x}{I}{_d}, \leftidx{_y}{I}{_d}]$ & $-$ & Damping and inertia
    parameters along the X and Y axes of the end-effector reference frame, used
    during closing the gripper, thus inducing force-controlled relaxation of
    tensions between the grasped object and the manipulator\\
 & & \textbf{Effectors constant parameters} \\
\hline
$\leftidx{^c}{c}{_1}[d_m]$ & $-$ & Maximum distance between the two fingers of
    the gripper (when the gripper is fully open)\\
$\leftidx{^c}{c}{_1}[c_l]$ & $-$ & Limit of the measured gripper motor current,
    indicating that the fingertips have tightened on the object \\
$\leftidx{^c}{c}{_1}[\leftidx{^E_G}{T}]$ & $-$ & Constant transformation
    representing the pose of the center of the gripper (G) w.r.t. end-effector
    (E) \\
 & & \textbf{Task configuration parameters} \\
\hline
$\leftidx{^c}{c}{_1}[p_x]$ & $-$ & Palette width in meters\\
$\leftidx{^c}{c}{_1}[p_y]$ & $-$ & Palette length in meters\\
$\leftidx{^c}{c}{_1}[n_x]$ & $-$ & Number of objects to be distributed in a
single row in the palette\\
$\leftidx{^c}{c}{_1}[n_y]$ & $-$ & Number of objects to be distributed in a
single column in the palette\\
 & & \textbf{Task variables} \\
\hline
$\leftidx{^c}{c}{_1}[n_o]$ & $-$ & Number of objects in the palette\\
$\leftidx{^c}{c}{_1}[\leftidx{^B_{1..V}}{O}{_c}]$ & $-$ & A set of verified
    object hypotheses w.r.t. the robot base\\
$\leftidx{^c}{c}{_1}[\leftidx{^B_t}{O}{_c}]$ & $-$ & Current target object
    w.r.t. the robot base. It might not be contained in
    $\leftidx{^c}{c}{_1}[\leftidx{^B_{1..V}}{O}{_c}]$ when the Kinect sensor
    is occluded during the pick phase\\
$\leftidx{^c}{c}{_1}[d_o]$ & $-$ & Desired distance between the two fingers of
    the gripper, calculated on the basis of object dimensions\\
$\leftidx{^c}{c}{_1}[\leftidx{^B_{E_a}}{T}{_d}]$ & $-$ & Target approach pose.
    Desired pose of the end-effector (E) w.r.t. robot base reference frame (B)
    indicating where the force-controlled target approach phase will begin\\
$\leftidx{^c}{c}{_1}[\leftidx{^B_{E_t}}{T}{_d}]$ & $-$ & Target pose. Desired pose
    of the end-effector (E) w.r.t. robot base reference frame (B) in which the
    target position has been reached and a contact with an object might occur\\
\end{tabular}
\caption{Memory of the control subsystem}
\label{table:memory}
\end{table}

\autoref{itm:task-goal} fixes the value for the task configuration parameters
defined in \autoref{table:memory}. The fixed values are summarized in \autoref{table:task-config}.

\begin{table}
\centering
\begin{tabular}{|c|c|}
\hline
Parameter & Value \\
\hline
\hline
$p_x$ & $1$\\
\hline
$p_y$ & $1$\\
\hline
$n_x$ & $25$\\
\hline
$n_y$ & $25$\\
\hline
\end{tabular}
\caption{Task configuration parameters fixed by the task description}
\label{table:task-config}
\end{table}

\begin{table}
\centering
\begin{tabular}{rcp{0.6\linewidth}}
 & & \textbf{Output to the manipulator $e_{1,m}$ for Point To Point motion} \\
\hline
$\leftidx{^e_y}{c}{_{1,m}}[\leftidx{^B_E}{T}{_d}]$ & $-$ & Desired Cartesian
    pose of the end-effector (E) w.r.t. the robot base (B) (homogeneous
    matrix)\\
 & & \textbf{Output to the manipulator $e_{1,m}$ for position-force control}
\\
\hline
$\leftidx{^e_y}{c}{_{1,m}}[b]$ & $-$ & 6 element vector containing the
    operation modes of the position-force controllers. Each regulator can have
    a different mode of operation: 
    \begin{itemize}
    \item $\mathfrak{u}$ - Unguarded motion (pure position control, without
        interaction with the environment)
    \item $\mathfrak{c}$ - Pure force control (contact with the environment)
    \item $\mathfrak{g}$ - Guarded motion (position control anticipating
        contact)
    \end{itemize} \\
$\leftidx{^e_y}{c}{_{1,m}}[F_d]$ & $-$ & 6 element vector of the desired
    force/torque exerted by the manipulator. Units of $N$ for the translational motion
    components (the 3 first elements) and $N\cdot m$ for the rotational motion
    components \\
$\leftidx{^e_y}{c}{_{1,m}}[V_d]$ & $-$ & 6 element vector of the desired
    manipulator velocity. Units of $\frac{m}{s}$ for the translational motion
    components (the 3 first elements) and $\frac{1}{s}$ for the rotational motion
    components\\
$\leftidx{^e_y}{c}{_{1,m}}[D_d]$ & $-$ & 6 element vector of the desired value
    of damping. Units of $\frac{kg}{s}$ for the translational motion
    components (the 3 first elements) and $\frac{kg\cdot m^2}{s}$ for the rotational motion
    components \\
$\leftidx{^e_y}{c}{_{1,m}}[I_d]$ & $-$ & 6 element vector of the desired value
    of inertia. Units of $kg$ for the translational motion
    components (the 3 first elements) and $kg\cdot m^2$ for the rotational motion
    components \\
 & & \textbf{Input from the manipulator $e_{1,m}$} \\
\hline
$\leftidx{^e_x}{c}{_{1,m}}[\leftidx{^B_E}{T}{_c}]$ & $-$ & Current Cartesian
    pose of the end-effector E w.r.t. the robot base (B) (homogeneous matrix)\\
$\leftidx{^e_x}{c}{_{1,m}}[\leftidx{^E}{F}{_c}]$ & $-$ & Current force/torque
    exerted by the end-effector w.r.t. the end-effector reference frame E\\
\\
\end{tabular}
\caption{Control subsystem's buffers related to the 6dof manipulator}
\label{table:manipulator-buffers}
\end{table}

\begin{table}
\centering
\begin{tabular}{rcp{0.6\linewidth}}
 & & \textbf{Output to gripper $e_{1,g}$} \\
\hline
$\leftidx{^e_y}{c}{_{1,g}}[d_d]$ & $-$ & 
    Desired gripper pose (distance between the jaws)\\
 & & \textbf{Input from gripper $e_{1,g}$} \\
\hline
$\leftidx{^e_x}{c}{_{1,g}}[d_c]$ & $-$ & 
    Current distance between the gripper jaws\\
$\leftidx{^e_x}{c}{_{1,g}}[c_c]$ & $-$ & 
    Measured motor current, proportional to the force exerted by the jaws on
    the grasped object\\
\\
\end{tabular}
\caption{Control subsystem's buffers related to the two-state gripper}
\label{table:gripper-buffers}
\end{table}

\begin{table}
\centering
\begin{tabular}{rcp{0.6\linewidth}}
 & & \textbf{Input from the Kinect sensor $r_{1,k}$} \\
\hline
$\leftidx{^r_x}{c}{_{1,k}}[\leftidx{^K_{1..V}}{O}{_c}]$ & $-$ & List of
    verified object hypotheses w.r.t. the Kinect reference frame. Every object
    $\leftidx{^K_v}{O}{_c}, v=1,...,V$ is a tuple, representing a single
    verified hypothesis, with respect to the Kinect frame K.
    \begin{center}
        $\leftidx{^K_v}{O}{_c} = \left(\leftidx{^K_v}{T}{_c},
        \leftidx{^K_v}{t}{_c}, \leftidx{^K_v}{C}{^{RGB}_c},
        \leftidx{^K_v}{C}{^{SIFT}_c}, \leftidx{_v}{c}{_c},
        \leftidx{_v}{i}{_c}\right)$
    \end{center}
    \begin{tabular}{rcp{0.72\linewidth}}
    $\leftidx{^K_v}{T}{_c}$&$-$&Object pose (homogeneous matrix)\\
    $\leftidx{^K_v}{t}{_c}$&$-$&Object instantaneous velocity. 6 element vector
        containing the linear and rotational velocity vectors concatenated\\
    $\leftidx{^K_v}{C}{^{RGB}_c}$&$-$&Object model dense colour cloud projected
        onto the scene w.r.t. Kinect frame\\
    $\leftidx{^K_v}{C}{^{SIFT}_c}$&$-$&Object model sparse cloud of \emph{Scale
        Invariant Feature Transform} features projected onto the scene w.r.t.
        Kinect frame\\
    $\leftidx{_v}{c}{_c}$&$-$&Object recognition confidence\\
    $\leftidx{_v}{i}{_c}$&$-$&Object label or identifier\\
    \end{tabular} \\
\\
\end{tabular}
\caption{Control subsystem's buffers related to the Kinect sensor}
\label{table:kinect-buffers}
\end{table}

\begin{table}
\centering
\begin{tabular}{rcp{0.6\linewidth}}
 & & \textbf{Input from the external agent that informs about the palette state} \\
\hline
$\leftidx{^T_x}{c}{_{1,p}}[a]$ & $-$ & Boolean, whether the system is active or
    not. When the external agent issues the \texttt{START} command mentioned in
    \autoref{itm:task-conditions}, the $a$ variable will be changed into
    \texttt{TRUE}. Correspondingly, the \texttt{STOP} command changes the
    variable into \texttt{FALSE} \\
\\
\end{tabular}
\caption{Control subsystem's buffers related to communication with external agents}
\label{table:communication-buffers}
\end{table}
\clearpage