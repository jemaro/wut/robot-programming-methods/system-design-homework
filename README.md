# System Design Homework

The  task  of  the  system  is  to  detect red cubes moving on  a  conveyor
having a white tape, at a  velocity  not  greater  than  0.05m/s.  The  cubes
having  dimensions  of 3cm ±0.5cm have  to  be  grasped  and placed on a
palette 100cm x 100cm

## Feedback
Points 45/50. Maximum grade 46. Minimum 9. Mean 35.


For example, in your homework  the virtual effector period in 2ms but control
subsystem 1ms. What is the reason that the control subsystem must run twice as
fast as the virtual effectors? New data from the virtual receptor are obtained
only every 33ms. Also it is not clear what is the initial state of the control
subsystem FSM? When the START command is received it should start in S_1,1 or
not?